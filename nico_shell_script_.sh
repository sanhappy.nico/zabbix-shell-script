#!/bin/bash

#安裝基礎套件
yum -y install epel-release
yum -y install vim net-tools telnet htop wget git zip unzip bind-utils bash-completion
#關閉系統防火牆與Selinux
systemctl stop firewalld
systemctl disable firewalld
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
#建立下載目錄
mkdir -p /data/dl

##安裝LNMP
#安裝Nginx
wget http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
rpm -ivh nginx-release-centos-7-0.el7.ngx.noarch.rpm
yum install nginx -y
#啟動Nginx
systemctl start nginx
#設定開機啟動Nginx
systemctl enable nginx

##MySQL
#安裝MySQL套件庫
rpm -Uvh https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm
#安裝MySQL
yum install mariadb-server -y
#啟動 MySQL
systemctl start mysqld
#預設開機啟動 MySQL
systemctl enable mysqld

##PHP
#安裝PHP
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
yum install php72w php72w-devel php72w-fpm php72w-gd php72w-mbstring php72w-mysql php72w-bcmath php72w-xml php72w-cli php72w-common php72w-ldap -y
#檢查PHP版本
php -v
#修改php-fpm配置Nginx
cp /etc/php-fpm.d/www.conf /etc/php-fpm.d/www.conf.backup
sed -i 's/user = apache/user = nginx/g' /etc/php-fpm.d/www.conf
sed -i 's/group = apache/group = nginx/g' /etc/php-fpm.d/www.conf
#修改php配置使其適合zabbix
cp /etc/php.ini /etc/php.ini.backup
sed -i 's/expose_php = on/expose_php = Off/g' /etc/php.ini                  #隱藏PHP版本
sed -i 's/max_execution_time = 30/max_execution_time = 300/g' /etc/php.ini  #監控執行時間
sed -i 's/max_input_time = 60/max_input_time = 300/g' /etc/php.ini          #接收數據等待時間
sed -i 's/memory_limit = 128M/memory_limit = 128M/g' /etc/php.ini           #每個腳本占用內存
sed -i 's/post_max_size = 8M/post_max_size = 16M/g' /etc/php.ini            #POST數據大小
sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 2M/g' /etc/php.ini #下載文件大小
sed -i 's/;date.timezone =/date.timezone = Asia\/Taipei/g' /etc/php.ini      #將時區設為台北時區
sed -i '800ialways_populate_raw_post_data = -1' /etc/php.ini              
#可以用$HTTP_RAW_POST_DATA接收post raw data(注意，這行是額外添加進去的，請在第800行處新增)

#配置PHP請求被傳送到後端的php-fpm模塊
cp /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.backup
sed -i '36i\
index  index.php index.html index.htm;\
\
location ~ \.php$ {\
    root           /usr/share/nginx/html;\
    fastcgi_pass   127.0.0.1:9000;\
    fastcgi_index  index.php;\
    fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;\
    include        fastcgi_params;\
}' /etc/nginx/conf.d/default.conf
#檢查Nginx配置
nginx -t
#重新加載Nginx
nginx -s reload
#啟動 PHP-FPM
systemctl start php-fpm.service
#預設開機啟動 PHP-FPM
systemctl enable php-fpm.service

##Zabbix-Server
#安裝Zabbix-Server依賴庫
yum -y install gcc gcc-c++ unixODBC-devel mysql-devel net-snmp-devel libevent libevent-devel libxml2-devel libcurl libcurl-devel java-1.6.0-openjdk-devel fping curl-devel libxml2 snmpd net-snmp
#建立Zabbix-Server使用者
useradd -U zabbix
#下載Zabbix5.0源碼包
cd /data/dl
wget https://cdn.zabbix.com/zabbix/sources/stable/5.0/zabbix-5.0.19.tar.gz
#解壓縮
tar -zxvf zabbix-5.0.19.tar.gz
#安裝編譯，並將Zabbix-Server安裝至/data底下
cd /data/dl/zabbix-5.0.19
#注意：這邊–prefix與–with-mysql需要依照實際需求改成對應的路徑
./configure --prefix=/data/zabbix-server --enable-server --enable-agent --enable-proxy --with-mysql=/usr/bin/mysql_config --with-net-snmp --with-libcurl --with-libxml2 --with-unixodbc --enable-java
make && make install
#配置zabbix_server.conf配置文件
cp /data/zabbix-server/etc/zabbix_server.conf /data/zabbix-server/etc/zabbix_server.conf.backup
sed -i 's/# DBPassword=/DBPassword=Zabbix0000!/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# DBSocket=/DBSocket=\/var\/lib\/mysql\/mysql.sock/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# DBPort=/DBPort=3306/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/LogFile=\/tmp\/zabbix_server.log/LogFile=\/data\/zabbix-server\/zabbix_server.log/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# LogFileSize=1/LogFileSize=0/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# PidFile=\/tmp\/zabbix_server.pi/PidFile=\/data\/zabbix-server\/zabbix_server.pid/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# SocketDir=\/tmp/SocketDir=\/data\/zabbix/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# SNMPTrapperFile=\/tmp\/zabbix_traps.tmp/SNMPTrapperFile=\/data\/snmptrap/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# AlertScriptsPath=${datadir}\/zabbix\/alertscripts/AlertScriptsPath=\/data\/zabbix-server\/share\/zabbix\/alertscripts/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# ExternalScripts=${datadir}\/zabbix\/externalscripts/ExternalScripts=\/data\/zabbix-server\/share\/zabbix\/externalscripts/g' /data/zabbix-server/etc/zabbix_server.conf
#建立zabbix-server需要的文件目錄
mkdir -p /data/zabbix
mkdir -p /data/snmptrap
#配置zabbix-server前端界面
mkdir -p /usr/share/nginx/html/zabbix
cd /data/dl/zabbix-5.0.19/ui
cp -a * /usr/share/nginx/html/zabbix/
#變更目錄權限
chown -R zabbix:zabbix /data/zabbix-server
chown -R zabbix:zabbix /data/zabbix
chown -R zabbix:zabbix /data/snmptrap
chown -R zabbix:zabbix /usr/share/nginx/
chmod -R 777 /var/lib/php/session/

##Zabbix-Agent
#修改Zabbix-Agent配置
cp /data/zabbix-server/etc/zabbix_agentd.conf /data/zabbix-server/etc/zabbix_agentd.conf.backup
sed -i 's/# UnsafeUserParameters=0/UnsafeUserParameters=1/g' /data/zabbix-server/etc/zabbix_agentd.conf
#啟動zabbix-server
/data/zabbix-server/sbin/zabbix_server -c /data/zabbix-server/etc/zabbix_server.conf
#啟動zabbix-agent
/data/zabbix-server/sbin/zabbix_agentd -c /data/zabbix-server/etc/zabbix_agentd.conf
#重啟php-fpm.service
systemctl restart php-fpm.service
#重啟Nginx
systemctl restart nginx
